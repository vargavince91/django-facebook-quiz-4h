# Test Task

**Write a performant web-application that does the following**:

* User is able to answer 10 different questions (out of a variable amount of questions) with multiple answers that are specific for these questions
* After answering all the questions, user is asked to log in via Facebook
* After successful login the results are saved to DB and linked to user's account
* User can then share a link with his friends on Facebook
* Their friends on Facebook then can answer the user's questions as well
* In the end we want to display to what percentage the user's friends matched the user's answers
* Their friends then get a message like: "You know your friend X very well, you answered 66% of all questions like them"
* Afterwards user can create their own friendship-test by answering 10 questions and sharing them on Facebook as well

You can use pluggable django apps, don’t write everything from scratch.

## General informations

* You have 4 hours time
* Please use Python/Django ~~(or another framework you will more comfortable with)~~ and ~~MySQL or~~ SQLite database
* Make sure to implement a clean, well-structured and high-performance database scheme
* Don't focus on graphical interfaces or layout
* Focus on database design and backend logic first and only if there is time left then also implement the Facebook login logics.
* Be efficient when writing your code, use OOP and loosely coupled classes as much as possible
* ~~It’s up to you to commit your code to a repository but if you do,~~ pay attention to clean commits
* It’s up to you to write unittests for this test-task