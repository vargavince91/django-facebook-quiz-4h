# `django-facebook-quiz-4h`

> Short coding challenge where I (hopefully) implement a Facebook quiz app with Django within 4 hours.

## Intro

You can see the instruction set for this coding challenge in [`INSTRUCTIONS.md`](INSTRUCTIONS.md).

## Development environment

1. For a basic setup, make sure Python and pip is installed.

    ```
    $ python --version
    Python 3.6.2
    $ 
    ```

2. Create a virtual environment and activate it. You can use for example with `venv` (anything else is good, could also create container, or virtual machine, but I got 4 hours :))

    ```
    $ python -m venv venv
    $ source venv/bin/activate
    (venv) $
    ```
    
3. `pip install -r requirements.txt`

