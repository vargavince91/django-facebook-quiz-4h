from django.db import models


class User(models.Model):
    username = models.CharField(max_length=64)
    # I am not sure yet how Facebook Authentication is going to work out.
    # Do we get an ID like email and a secret token? For now, I assume that.
    email = models.EmailField()
    token = models.CharField(max_length=128)


class Question(models.Model):
    question_text = models.CharField(max_length=512)

    def __str__(self):
        return '#{}: "{}"'.format(self.pk, self.question_text)


class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=512)

    def __str__(self):
        return '#{}: "{}" <Q#{}>'.format(
            self.pk,
            self.choice_text,
            self.question.pk,
        )


class Quiz(models.Model):
    user = models.ForeignKey(User)
    question = models.ForeignKey(Question)
    answer = models.ForeignKey(Choice)

    class Meta:
        verbose_name_plural = 'Quizzes'

    def __str__(self):
        return '<user: {}> Q: {} A: {}'.format(
            self.user.username,
            self.question.question_text,
            self.answer.choice_text
        )
