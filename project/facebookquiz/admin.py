from django.contrib import admin

from .models import User, Question, Quiz, Choice

admin.site.register((User, Question, Quiz, Choice))
